Array = {}
function Array:new (size)
o = {}
o.size = size
for i=1, size do
o[i] = nil
end
setmetatable(o, self)
self.__index = self
return o
end

-- METEOR """"""""""""CLASS""""""""""""
Meteor = {}
function Meteor:new (radius, speed)
o = {}
o.posx = radius + love.math.random() * (love.graphics.getWidth() - radius * 2)
o.posy = -radius
o.radius = radius
o.speed = speed
o.r = 110 + love.math.random() * 40
o.g = 70 + love.math.random() * 40
o.b = 40 + love.math.random() * 40
setmetatable(o, self)
self.__index = self
return o
end
-- METEOR """"""""""""CLASS""""""""""""

-- RELOAD GAME
function love.reload()
spaceship = love.graphics.newImage("spaceship.png")
bgposx = (love.graphics.getWidth() / 2) - (background:getWidth() / 2)
bgposy = -background:getHeight() / 2
posx = (love.graphics.getWidth() / 2) - (spaceship:getWidth() / 2)
posy = (love.graphics.getHeight() / 2) - (spaceship:getHeight() / 2)
meteorSpawnRate = 5
meteorSpawnCd = meteorSpawnRate
playerAlive = true
meteorList = Array:new(100)
keytipsCd = 5
survivalTime = 0
end
-- RELOAD GAME

-- LOAD GRAPHICS
function love.loadGraphics()
font = love.graphics.newFont(30)
love.graphics.setFont(font)
spaceship = love.graphics.newImage("spaceship.png")
background = love.graphics.newImage("background.png")
keytips = love.graphics.newImage("keytips.png")
resettip = love.graphics.newImage("resettip.png")
bgposx = (love.graphics.getWidth() / 2) - (background:getWidth() / 2)
bgposy = -background:getHeight() / 2
posx = (love.graphics.getWidth() / 2) - (spaceship:getWidth() / 2)
posy = (love.graphics.getHeight() / 2) - (spaceship:getHeight() / 2)
playerHalfWidth = spaceship:getWidth() / 2
playerHalfHeight = spaceship:getHeight() / 2
end
-- LOAD GRAPHICS

-- GAME PARAMETERS
function love.setGameParameters()
survivalTime = 0
keytipsCd = 5
meteorMinRadius = 25
meteorMaxRadius = 75
meteorMinSpeed = 75
meteorMaxSpeed = 150
meteorSpawnRate = 5
meteorSpawnDecay = 0.1
meteorMinSpawnRate = 0.5
meteorSpawnCd = meteorSpawnRate
parallaxSpeed = 200
speed = 300
originalSpeed = speed
playerAlive = true
playerHitboxRadius = 24
screenHeight = love.graphics.getHeight()
meteorList = Array:new(100)
end
-- GAME PARAMETERS

-- GET RANDOM RANGE
function love.randomRange(rmin,rmax)
r = rmax - rmin
r = love.math.random() * r
r = rmin + r
return r
end
-- GET RANDOM RANGE

-- LOAD
function love.load()
love.loadGraphics()
love.setGameParameters()
end
-- LOAD

-- CHECK INPUT
function love.checkInput(dt)
if love.keyboard.isDown("lshift") then
speed = originalSpeed * 0.5
else
speed = originalSpeed
end
if love.keyboard.isDown("up") and love.keyboard.isDown("right") then
posx = posx + 0.707 * speed * dt
posy = posy - 0.707 * speed * dt
elseif love.keyboard.isDown("up") and love.keyboard.isDown("left") then
posx = posx - 0.707 * speed * dt
posy = posy - 0.707 * speed * dt
elseif love.keyboard.isDown("down") and love.keyboard.isDown("right") then
posx = posx + 0.707 * speed * dt
posy = posy + 0.707 * speed * dt
elseif love.keyboard.isDown("down") and love.keyboard.isDown("left") then
posx = posx - 0.707 * speed * dt
posy = posy + 0.707 * speed * dt
elseif love.keyboard.isDown("up") then
posy = posy - 1 * speed * dt
elseif love.keyboard.isDown("down") then
posy = posy + 1 * speed * dt
elseif love.keyboard.isDown("left") then
posx = posx - 1 * speed * dt
elseif love.keyboard.isDown("right") then
posx = posx + 1 * speed * dt
end
end
-- CHECK INPUT

-- CHECK SCREEN BOUNDS
function love.checkScreenBounds()
if(posx < 0) then
posx = 0
end
if (posx + spaceship:getWidth() > love.graphics.getWidth()) then
posx = love.graphics.getWidth() - spaceship:getWidth()
end
if (posy < 0) then
posy = 0
end
if (posy + spaceship:getHeight() > love.graphics.getHeight()) then
posy = love.graphics.getHeight() - spaceship:getHeight()
end
end
-- CHECK SCREEN BOUNDS

-- PARALLAX BACKGROUND
function love.parallaxBackground(dt)
bgposy = bgposy + parallaxSpeed * dt
if bgposy > 0 then
bgposy = -background:getHeight() / 2
end
end
-- PARALLAX BACKGROUND

-- MOVE METEORS
function love.moveMeteors(dt)
for i=1, meteorList.size do
if meteorList[i] ~= nil then
meteorList[i].posy = meteorList[i].posy + meteorList[i].speed * dt
end
end
end
-- MOVE METEORS

-- CIRCLE OVERLAP
function love.boundingCircleOverlap(aPosx, aPosy, bPosx, bPosy, aRadius, bRadius)
if (math.sqrt((aPosx - bPosx) * (aPosx - bPosx) + (aPosy - bPosy) * (aPosy - bPosy))) - (aRadius + bRadius) <= 0 then return true
else return false
end
end
-- CIRCLE OVERLAP

-- COLLISION CHECK
function love.collisionCheck()
for i=1, meteorList.size do
if meteorList[i] ~= nil then
if love.boundingCircleOverlap(posx + playerHalfWidth, posy + playerHalfHeight, meteorList[i].posx, meteorList[i].posy, playerHitboxRadius, meteorList[i].radius) then
return true
end
end
end
return false
end
-- COLLISION CHECK

-- METEOR OUTSIDE OF SCREEN
function love.checkOutOfScreenMeteors()
for i=1, meteorList.size do
if meteorList[i] ~= nil then
if meteorList[i].posy > love.graphics.getHeight() + meteorList[i].radius then
meteorList[i] = nil
end
end
end
end
-- METEOR OUTSIDE OF SCREEN

-- GENERATE METEORS
function love.generateMeteor()
for i=1, meteorList.size do
if meteorList[i] == nil then
meteorList[i] = Meteor:new(love.randomRange(meteorMinRadius, meteorMaxRadius),love.randomRange(meteorMinSpeed, meteorMaxSpeed))
break
end
end
end
-- GENERATE METEORS

-- ROUND NUMBER
function love.round(num, numDecimalPlaces)
local mult = 10^(numDecimalPlaces or 0)
return math.floor(num * mult + 0.5) / mult
end
-- ROUND NUMBER

-- UPDATE
function love.update(dt)
if playerAlive then
survivalTime = survivalTime + dt
end
if keytipsCd > 0 then
keytipsCd = keytipsCd - dt
end
if meteorSpawnCd > 0 then
meteorSpawnCd = meteorSpawnCd - dt
else
love.generateMeteor()
meteorSpawnCd = meteorSpawnRate
end
if meteorSpawnRate > meteorMinSpawnRate then
meteorSpawnRate = meteorSpawnRate - meteorSpawnDecay * dt
end
love.checkOutOfScreenMeteors()
love.parallaxBackground(dt)
love.checkInput(dt)
love.checkScreenBounds()
love.moveMeteors(dt)
if love.collisionCheck() then
playerAlive = false
end
if not playerAlive then
if love.keyboard.isDown("r") then
love.reload()
end
end
end
-- UPDATE

-- DRAW
function love.draw()
love.graphics.draw(background, bgposx, bgposy)
if playerAlive then
love.graphics.draw(spaceship, posx, posy)
end
for i=1, meteorList.size do
if meteorList[i] ~= nil then
love.graphics.setColor(meteorList[i].r, meteorList[i].g, meteorList[i].b)
love.graphics.circle("fill", meteorList[i].posx, meteorList[i].posy, meteorList[i].radius)
love.graphics.setColor(255, 255, 255)
end
end
if keytipsCd > 0 then
love.graphics.draw(keytips,0,0)
end
if not playerAlive then
love.graphics.draw(resettip,0,0)
end
love.graphics.print("Survival time: " .. love.round(survivalTime,1), 235, 0, 0, 1.25, 1.25, 0, 0)
end
-- DRAW
